/**
 * @namespace Chart
 */
import Chart from './core/core.controller';

Chart.helpers = require('./helpers/index').default;
Chart._adapters = require('./core/core.adapters').default;
Chart.Animation = require('./core/core.animation').default;
Chart.Animator = require('./core/core.animator').default;
Chart.animationService = require('./core/core.animations').default;
Chart.controllers = require('./controllers/index').default;
Chart.DatasetController = require('./core/core.datasetController').default;
Chart.defaults = require('./core/core.defaults').default;
Chart.Element = require('./core/core.element').default;
Chart.elements = require('./elements/index').default;
Chart.Interaction = require('./core/core.interaction').default;
Chart.layouts = require('./core/core.layouts').default;
Chart.platform = require('./platforms/platform').default;
Chart.plugins = require('./core/core.plugins').default;
Chart.Scale = require('./core/core.scale').default;
Chart.scaleService = require('./core/core.scaleService').default;
Chart.Ticks = require('./core/core.ticks').default;
Chart.Tooltip = require('./core/core.tooltip').default;

// Register built-in scales
import scales from './scales';
Object.keys(scales).forEach(function(type) {
	const scale = scales[type];
	Chart.scaleService.registerScaleType(type, scale, scale._defaults);
});

// Load to register built-in adapters (as side effects)
import './adapters';

// Loading built-in plugins
import plugins from './plugins';
for (var k in plugins) {
	if (Object.prototype.hasOwnProperty.call(plugins, k)) {
		Chart.plugins.register(plugins[k]);
	}
}

Chart.platform.initialize();

export default Chart;
if (typeof window !== 'undefined') {
	window.Chart = Chart;
}
